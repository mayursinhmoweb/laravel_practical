<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::paginate(10);
        return view('company',compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allinout = $request->input();
        unset($allinout['_token']);
        // echo '<pre>';
        // print_r($request);
        // exit;
        if(!empty($request->file('logo'))){
            $original_pic = $request->file('logo');

            $file_extension=$original_pic->getClientOriginalExtension();
            $filename = time() . '.' . $file_extension;

            // # upload original image
            Storage::put('public/logos/' . $filename, (string) file_get_contents($original_pic), 'public');

            $allinout['logo'] = $filename;
        }
        else{
            $allinout['logo'] = "";
        }
        /*
        if(!empty($request->file('logo'))){
            $path = $request->file('logo')->store(
                'logos', 'public'
            );
            $allinout['logo'] = $path;
        }else{
            $allinout['logo'] = "";
        }
        */
        Company::create($allinout);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $allinout = $request->input();
        $id = $allinout['id'];
        unset($allinout['_token']);
        unset($allinout['id']);
        // if(!empty($request->file('logo'))){
            // $path = $request->file('logo')->store(
            //     'logos', 'public'
            // );
            // $file = $request->file('logo');
            // dd($file);
            // $name = $this->getName($file);
            // $path = 'public/logos';

            try {
                //code...
                // $uplaodedFileName = Storage::disk('local')->put($path, $file);

                // Storage::put($path, $file);
                // $path = $request->file('logo')->store($path);
                //$path =Storage::disk('local') -> put($name, file_get_contents($file->getRealPath()));
                if($request->file('logo')){
                    $original_pic = $request->file('logo');

                    $file_extension=$original_pic->getClientOriginalExtension();
                    $filename = time() . '.' . $file_extension;

                    // # upload original image
                    Storage::put('public/logos/' . $filename, (string) file_get_contents($original_pic), 'public');

                    $allinout['logo'] = $filename;
                }


            } catch (\Throwable $th) {
                //throw $th;
                dd($th);
            }


        // }else{
        //     unset($allinout['logo']);
        // }

        Company::where('id',$id)->update($allinout);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Company::where('id',$id)->delete();
        return back();
    }
    private function getName($file)
    {
        return Str::slug($file->getClientOriginalName()) . '-' . time() . '.' . $file->getClientOriginalExtension();
    }
}
