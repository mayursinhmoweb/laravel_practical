@extends('layouts.app')


@section('content')
<!-- container section start -->
<section id="container" class="">
@extends('layouts.header')
<!--main content start-->
<section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> Employee</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
              <li><i class="fa fa-laptop"></i>Employee</li>
            </ol>
          </div>
        </div>
        <div class="row">

          <div class="col-lg-9 col-md-12">
              <div>
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#employeeModal">
                Add new
              </button>
              </div>
              <div class="panel-body">
                <table class="table bootstrap-datatable countries">
                  <thead>
                    <tr>
                      <th>First name</th>
                      <th>Last Name</th>
                      <th>Company Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($employee as $key => $val)
                    <tr>
                      <td>{{$val->firstname}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->companyName->name}}</td>
                      <td>{{$val->email}}</td>
                      <td>{{$val->phone}}</td>
                      <td><a href="javascript:void(0);" class="editEmployee" data-companyid="{{$val->company_id}}"  data-id="{{$val->id}}" data-fname="{{$val->firstname}}" data-lname="{{$val->lastname}}" data-email="{{$val->email}}" data-phone="{{$val->phone}}" >Edit</a> &nbsp;|&nbsp; <a href="{{route('delete_employee',[$val->id])}}" class="deleteEmp" data-id="{{$val->id}}">Delete</a> </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {{ $employee->links() }}
              </div>

            </div>

          </div>

        </div>



        <!-- statics end -->
        </section>
</section>

<!-- Modal -->
<div class="modal fade" id="employeeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="{{route('save_employee')}}" enctype="multipart/form-data">
      @csrf
        <div class="form-group">
          <label for="exampleInputname1">Select Company</label>
          <select name="company_id" id="company_id" class="form-control">
            <option value="0">Select</option>
            @foreach($company as $key => $val)
              <option value="{{$val->id}}">{{$val->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputname1">First Name</label>
          <input type="text" class="form-control" id="exampleInputcompany1" name="firstname" aria-describedby="emailHelp" placeholder="Enter first name" required>
        </div>
        <div class="form-group">
          <label for="exampleInputname1">Last Name</label>
          <input type="text" class="form-control" id="exampleInputcompany1" name="lastname" aria-describedby="emailHelp" placeholder="Enter last name" required>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Email</label>
          <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Enter email" required>
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
          <label for="exampleInputname1">Phone</label>
          <input type="text" class="form-control numericval" id="phone" name="phone" aria-describedby="phoneHelp" placeholder="Enter phone" required>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<!-- Update Modal -->
<div class="modal fade" id="updateEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Company</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="{{route('update_employee')}}" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
          <label for="exampleInputname1">Select Company</label>
          <select name="company_id" id="update_company_id" class="form-control">
            <option value="0">Select</option>
            @foreach($company as $key => $val)
              <option value="{{$val->id}}">{{$val->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputname1">First Name</label>
          <input type="hidden" name="id" id="id" value="">
          <input type="text" class="form-control" id="updatefirstname" name="firstname" aria-describedby="emailHelp" placeholder="Enter first name" required>
        </div>
        <div class="form-group">
          <label for="exampleInputname1">Last Name</label>
          <input type="text" class="form-control" id="updatelastname" name="lastname" aria-describedby="emailHelp" placeholder="Enter last name" required>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Email</label>
          <input type="email" class="form-control" id="updateemail" name="email" aria-describedby="emailHelp" placeholder="Enter email" required>
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
          <label for="exampleInputname1">Phone</label>
          <input type="number" class="form-control numericval" id="updatephone" name="phone" aria-describedby="emailHelp" placeholder="Enter website" required>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
@endsection
