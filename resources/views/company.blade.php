@extends('layouts.app')


@section('content')
<!-- container section start -->
<section id="container" class="">
@extends('layouts.header')
<!--main content start-->
<section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> Company</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
              <li><i class="fa fa-laptop"></i>Company</li>
            </ol>
          </div>
        </div>
        <div class="row">

          <div class="col-lg-9 col-md-12">
              <div>
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Add new
              </button>
              </div>
              <div class="panel-body">
                <table class="table bootstrap-datatable countries">
                  <thead>
                    <tr>
                      <th>Logo</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>WebSite</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($company as $key => $val)
                    <tr>
                      <td><img src="<?php echo asset('storage/logos/'.$val->logo); ?>" style="height:18px; margin-top:-2px;"></td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->email}}</td>
                      <td>{{$val->website}}</td>
                      <td><a href="javascript:void(0);" class="edit" data-id="{{$val->id}}" data-name="{{$val->name}}" data-email="{{$val->email}}" data-website="{{$val->website}}" data-id="{{$val->website}}">Edit</a> &nbsp;|&nbsp; <a href="{{route('delete_company',[$val->id])}}" class="deleteComp" data-id="{{$val->id}}">Delete</a> </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {{ $company->links() }}
              </div>
            </div>
          </div>
        </div>



        <!-- statics end -->
        </section>
</section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Company</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="{{route('save_company')}}" enctype="multipart/form-data">
      @csrf
        <div class="form-group">
          <label for="exampleInputname1">Company Name</label>
          <input type="text" class="form-control" id="exampleInputcompany1" name="name" aria-describedby="emailHelp" placeholder="Enter company name" required>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Email</label>
          <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Enter email" required>
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
          <label for="exampleInputname1">Website</label>
          <input type="text" class="form-control" id="website" name="website" aria-describedby="emailHelp" placeholder="Enter website" required>
        </div>
        <div class="form-group">
          <label for="exampleInputname1">Logo</label>
          <input type="file" class="form-control" id="logo" name="logo" aria-describedby="logoHelp">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<!-- Update Modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Company</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="{{route('update_company')}}" enctype="multipart/form-data">
      @csrf
        <div class="form-group">
          <label for="exampleInputname1">Company Name</label>
          <input type="hidden" name="id" id="id" value="">
          <input type="text" class="form-control" id="updatecompanyname" name="name" aria-describedby="emailHelp" placeholder="Enter company name" required>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Email</label>
          <input type="email" class="form-control" id="updateemail" name="email" aria-describedby="emailHelp" placeholder="Enter email" required>
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
          <label for="exampleInputname1">Website</label>
          <input type="text" class="form-control" id="updatewebsite" name="website" aria-describedby="emailHelp" placeholder="Enter website" required>
        </div>
        <div class="form-group">
          <label for="exampleInputname1">Logo</label>
          <input type="file" class="form-control" id="logo" name="logo" aria-describedby="logoHelp">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
@endsection
