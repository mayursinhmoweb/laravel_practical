<?php
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['middleware' => ['auth']], function(){
    Route::get('company',[CompanyController::class,'index'])->name('company');
    Route::post('save-company',[CompanyController::class,'store'])->name('save_company');
    Route::post('update-company',[CompanyController::class,'update'])->name('update_company');
    Route::get('delete_company/{id}',[CompanyController::class,'destroy'])->name('delete_company');

    Route::get('employee',[EmployeeController::class,'index'])->name('employee');
    Route::post('save-employee',[EmployeeController::class,'store'])->name('save_employee');
    Route::post('update-employee',[EmployeeController::class,'update'])->name('update_employee');
    Route::get('delete_employee/{id}',[EmployeeController::class,'destroy'])->name('delete_employee');
});
